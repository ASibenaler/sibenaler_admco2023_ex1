#!/usr/bin/python3
# Simple TicTacToe / morpion game in Python 
from datetime import datetime

import random
import sys

 
class morpion_board:
    '''Classe simulant un jeu de morpion'''
    
    def __init__(self):
        '''initialisation de la grille de jeu'''
        self.board=[i for i in range(0,9)]
        #print(self.board)
        self.player1, self.player2 = '',''
        # Corners, Center and Others, respectively
        self.moves=((1,7,3,9),(5,),(2,4,6,8))
        # Winner combinations
        self.winners=((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6))
        # Table
        self.tab=range(1,10)
        
    def print_board(self):
        '''affichage de la grille'''
        x=1
        for i in self.board:
            end = ' | '
            if x%3 == 0:
                end = ' \n'
                if i != 1: end+='---------\n';
            char=' '
            if i in ('X','O'): char=i;
            x+=1
            #print(char,end=end)
            
    def select_char(self):
        '''selection aleatoire du type de pion'''
        chars=('X','O')
        if random.randint(0,1) == 0:
            return chars[::-1]
        return chars

    def can_move(self,player, move):
        '''methode verifiant si un mouvement est possible'''
        if move in self.tab and self.board[move-1] == move-1:
            return True
        return False

    def can_win(self,player, move):
        '''methode verifiant si un joueur a gagne'''
        places=[]
        x=0
        for i in self.board:
            if i == self.player1: places.append(x);
            x+=1
        win=True
        for tup in self.winners:
            win=True
            for ix in tup:
                if self.board[ix] != player:
                    win=False
                    break
            if win == True:
                break
        return win

    def make_move(self, player, move, undo=False):
        '''methode effectuant le mouvement passe en parametre'''
        if self.can_move(player, move):
            self.board[move-1] = player
            win=self.can_win(player, move)
            if undo:
                self.board[move-1] = move-1
            return (True, win)
        return (False, False)

    def random1_move(self):
        '''selection aleatoire d'un mouvement possible (joueur1)'''
        move=-1
        random.seed(datetime.now())
        # If I can win, others don't matter.
        for i in random.sample(range(1,10),9):
            if self.can_move(self.player1, i):
                move=i
                break
        return self.make_move(self.player1, move)

    def random2_move(self):
        '''selection aleatoire d'un mouvement possible (joueur2)'''
        move=-1
        random.seed(datetime.now())
        # If I can win, others don't matter.
        for i in random.sample(range(1,10),9):
            if self.can_move(self.player2, i):
                move=i
                break

        return self.make_move(self.player2, move)

    def human1_move(self):
        '''methode permetant d'entrer manuellement un mouvement (joueur1)'''
        self.print_board()
        print('# Make your move ! [1-9] : ', end='')
        move = int(input())
        return self.make_move(self.player1,move)

    def human2_move(self):
        '''methode permetant d'entrer manuellement un mouvement (joueur2)'''
        self.print_board()
        print('# Make your move ! [1-9] : ', end='')
        move = int(input())
        return self.make_move(self.player2,move)

    def computer1_move(self):
        '''choix automatique d'un case (ordi1)'''
        move=-1
        # If I can win, others don't matter.
        for i in range(1,10):
            if self.make_move(self.player1, i, True)[1]:
                move=i
                break
        if move == -1:
            # If other player can win, block him.
            for i in range(1,10):
                if self.make_move(self.player2, i, True)[1]:
                    move=i
                    break
        if move == -1:
            # Otherwise, try to take one of desired places.
            for tup in self.moves:
                l=list(tup)
                random.shuffle(l)
                for mv in l :
                    if move == -1 and self.can_move(self.player1, mv):
                        move=mv
                        break
        return self.make_move(self.player1, move)

    def computer2_move(self):
        '''choix automatique d'un case (ordi2)'''
        move=-1
        # If I can win, others don't matter.
        for i in range(1,10):
            if self.make_move(self.player2, i, True)[1]:
                move=i
                break
        if move == -1:
            # If player can win, block him.
            for i in range(1,10):
                if self.make_move(self.player1, i, True)[1]:
                    move=i
                    break
        if move == -1:
            # Otherwise, try to take one of desired places.
            for tup in (self.moves):
                l=list(tup)
                random.shuffle(l)
                for mv in l :
                    if move == -1 and self.can_move(self.player2,mv):
                        move=mv
                        break
        return self.make_move(self.player2, move)

    def space_exist(self):
        '''verifie s'il y a des cases libres'''
        return self.board.count('X') + self.board.count('O') != 9

    def playerAvsplayerB(self,playerA_move,playerB_move):
        ''''''
        self.player1='X'
        self.player2 = 'O'
        print('Player  A is [%s] and Player B is [%s]' % (self.player1, self.player2))
        result='%%% Deuce ! %%%'
        computer_first = True
        if computer_first==True :
            while self.space_exist():
                #self.print_board()
                if playerA_move()[1]:
                    
                    result='*** Congratulations ! Player A wins ! ***'
                    break
                #self.print_board()
                if playerB_move()[1]:
                    result='*** Congratulations ! Player B wins ! ***'
                    break;
        self.print_board()
        print(result)

game=morpion_board()
#game.playerAvsplayerB(game.human1_move,game.human2_move)
game.playerAvsplayerB(game.random1_move,game.random2_move)
#game.playerAvsplayerB(game.computer1_move,game.random2_move)
#game.playerAvsplayerB(game.random1_move,game.computer2_move)
#game.playerAvsplayerB(game.computer1_move,game.computer2_move)
#game.playerAvsplayerB(game.human1_move,game.computer2_move)
#game.playerAvsplayerB(game.computer1_move,game.human2_move)




